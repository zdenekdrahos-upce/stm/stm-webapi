<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI;

class StmAPIProxy implements IStmAPI
{
    /** @var \STM\Plugin\WebAPI\StmAPIProxy */
    private static $instance;

    /** @return \STM\Plugin\WebAPI\IStmAPI */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self(STM_WEBAPI_CONFIG, STM_WEBAPI_BOOTSTRAP_DIR);
        }

        return self::$instance;
    }

    /** @var string */
    private $configFile;
    /** @var string */
    private $bootstrapDir;
    /** @var \STM\Plugin\WebAPI\StmAPI */
    private $stmApi;

    private function __construct($jsonConfig, $bootstrapRootDir)
    {
        $this->configFile = $jsonConfig;
        $this->bootstrapDir = $bootstrapRootDir;
    }

    public function isUserWebAdministrator($idUser)
    {
        $this->loadApi();

        return $this->stmApi->isUserWebAdministrator($idUser);
    }

    public function competitions()
    {
        $this->loadApi();

        return $this->stmApi->competitions();
    }

    public function results()
    {
        $this->loadApi();

        return $this->stmApi->results();
    }

    public function match()
    {
        $this->loadApi();

        return $this->stmApi->match();
    }

    public function members()
    {
        $this->loadApi();

        return $this->stmApi->members();
    }

    public function transformations()
    {
        $this->loadApi();

        return $this->stmApi->transformations();
    }

    public function loadApi()
    {
        if (is_null($this->stmApi)) {
            $config = file_get_contents($this->configFile);
            $this->stmApi = new StmAPI($config, $this->bootstrapDir);
        }
    }
}
