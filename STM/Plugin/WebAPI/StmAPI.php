<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI;

use \InvalidArgumentException;

class StmAPI implements IStmAPI
{
    /** @var \STM\Plugin\WebAPI\Settings */
    private $stmSettings;
    /** @var \STM\Plugin\WebAPI\Match\IMatchFacade */
    private $match;
    /** @var \STM\Plugin\WebAPI\Members\IMembersFacade */
    private $members;
    /** @var \STM\Plugin\WebAPI\Results\IResultsFacade */
    private $results;
    /** @var \STM\Plugin\WebAPI\Transformations\ITransformationsFacade */
    private $transformations;

    /**
     * @param string $jsonConfig       json text
     * @param string $bootstrapRootDir path to bootstrap file defined in json
     */
    public function __construct($jsonConfig, $bootstrapRootDir = '')
    {
        $json = json_decode($jsonConfig);
        if (is_null($json)) {
            throw new InvalidArgumentException("Invalid JSON file");
        }
        $this->stmSettings = new Settings($json, $bootstrapRootDir);
    }

    public function isUserWebAdministrator($idUser)
    {
        return $this->stmSettings->isUserWebAdministrator($idUser);
    }

    public function competitions()
    {
        return $this->stmSettings->competitions();
    }

    public function results()
    {
        $this->initResults();

        return $this->results;
    }

    public function match()
    {
        $this->initMatch();

        return $this->match;
    }

    public function members()
    {
        $this->initMembers();

        return $this->members;
    }

    public function transformations()
    {
        $this->initTransformations();

        return $this->transformations;
    }

    private function initMatch()
    {
        if (is_null($this->match)) {
            if ($this->stmSettings->isStmRunning()) {
                $this->match = new Match\MatchFacade();
                $this->stmSettings->initStm();
            } else {
                $this->match = new Match\NullMatch();
            }
        }
    }

    private function initMembers()
    {
        if (is_null($this->members)) {
            if ($this->stmSettings->isStmRunning()) {
                $this->members = new Members\MembersFacade($this->stmSettings->getOrganizations());
                $this->stmSettings->initStm();
            } else {
                $this->members = new Members\NullMembers();
            }
        }
    }

    private function initTransformations()
    {
        if (is_null($this->transformations)) {
            $this->transformations = new Transformations\TransformationsFacade($this->stmSettings->getTransformations());
        }
    }

    private function initResults()
    {
        if (is_null($this->results)) {
            $competitions = $this->stmSettings->competitions();
            if ($this->stmSettings->isStmRunning()) {
                $this->results = new Results\ResultsFacade($competitions);
                $this->stmSettings->initStm();
            } else {
                $this->results = new Results\NullResults($competitions);
            }
        }
    }
}
