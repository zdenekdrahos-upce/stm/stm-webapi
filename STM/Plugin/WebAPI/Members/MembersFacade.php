<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Members;

use \stdClass;
use STM\Plugin\WebAPI\Members\Strategy\TeamMemberStrategy;
use STM\Plugin\WebAPI\Members\Strategy\ClubMemberStrategy;

class MembersFacade implements IMembersFacade
{
    /** @var stdClass */
    private $stdObject;
    /** @var \STM\Plugin\WebAPI\Members\IMembers */
    private $teamMembers;
    /** @var \STM\Plugin\WebAPI\Members\IMembers */
    private $clubMembers;

    public function __construct(stdClass $object)
    {
        $this->stdObject = $object;
    }

    public function getTeamMembers($idTeam = null)
    {
        $this->checkTeamMembers();

        return $this->teamMembers->getMembers($idTeam);
    }

    public function getClubMembers($idClub = null)
    {
        $this->checkClubMembers();

        return $this->clubMembers->getMembers($idClub);
    }

    private function checkTeamMembers()
    {
        if (is_null($this->teamMembers)) {
            $this->teamMembers = new Members(
                $this->stdObject->teams,
                new TeamMemberStrategy()
            );
        }
    }

    private function checkClubMembers()
    {
        if (is_null($this->clubMembers)) {
            $this->clubMembers = new Members(
                $this->stdObject->clubs,
                new ClubMemberStrategy()
            );
        }
    }

}
