<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Members;

class NullMembers implements IMembersFacade
{

    public function getTeamMembers($idTeam = null)
    {
        return array();
    }

    public function getClubMembers($idClub = null)
    {
        return array();
    }
}
