<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Members;

use \InvalidArgumentException;
use STM\Plugin\WebAPI\Members\Strategy\IMembersStrategy;

class Members
{
    /** @var int */
    private $defaultId;
    /** @var array */
    private $allowedId;
    /** @var \STM\Plugin\WebAPI\Members\IMembersStrategy */
    private $members;

    public function __construct(array $members, IMembersStrategy $strategy)
    {
        $this->members = $strategy;
        $this->allowedId = array_values($members);
        $this->defaultId = $this->allowedId[0];
    }

    public function getMembers($id = null)
    {
        $id = $this->getId($id);

        return $this->members->findAll($id);
    }

    private function getId($id)
    {
        $realId = is_null($id) ? $this->defaultId : $id;
        if (!is_int($realId) || !in_array($realId, $this->allowedId, true)) {
            throw new InvalidArgumentException('Invalid ID');
        }

        return $realId;
    }
}
