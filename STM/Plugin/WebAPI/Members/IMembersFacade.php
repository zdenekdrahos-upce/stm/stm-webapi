<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Members;

interface IMembersFacade
{
    /**
     * @param  int|null $idTeam default team is used if argument is null
     * @return array
     */
    public function getTeamMembers($idTeam = null);

    /**
     * @param  int|null $idClub default team is used if argument is null
     * @return array
     */
    public function getClubMembers($idClub = null);
}
