<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Members\Strategy;

use STM\StmFactory;

class ClubMemberStrategy implements IMembersStrategy
{
    public function findAll($id)
    {
        return StmFactory::find()->ClubMember->findByIdClub($id);
    }
}
