<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI;

use \stdClass;
use STM\Plugin\WebAPI\Competitions\CompetitionsFacade;

class Settings
{
    /** @var boolean */
    private $isRunning;
    /** @var boolean */
    private $isInitialized;
    /** @var string */
    private $bootstrap;
    /** @var array */
    private $webAdministrators;
    /** @var stdClass */
    private $stdCompetitions;
    /** @var \STM\Plugin\WebAPI\Competitions\ICompetitionsFacade */
    private $competitions;
    /** @var stdClass */
    private $stdOrganizations;
    /** @var stdClass */
    private $stdTransformations;

    public function __construct(stdClass $settings, $rootDir)
    {
        $this->isInitialized = false;
        $this->isRunning = $settings->isRunning;
        $this->bootstrap = $rootDir . $settings->bootstrap;
        $this->webAdministrators = $settings->webAdministrators;
        $this->stdOrganizations = $settings->organizations;
        $this->stdCompetitions = $settings->competitions;
        $this->stdTransformations = $settings->transformations;
    }

    /** @return boolean */
    public function isStmRunning()
    {
        return $this->isRunning === true;
    }

    public function initStm()
    {
        if ($this->isStmRunning() && !$this->isInitialized) {
            $this->isInitialized = true;
            require_once($this->bootstrap);
        }
    }

    public function isUserWebAdministrator($idUser)
    {
        if ($this->isStmRunning()) {
            return in_array($idUser, $this->webAdministrators, true);
        } else {
            return false;
        }
    }

    /** @return \STM\Plugin\WebAPI\Competitions\ICompetitionsFacade */
    public function competitions()
    {
        if (is_null($this->competitions)) {
            $this->competitions = new CompetitionsFacade($this->stdCompetitions);
        }

        return $this->competitions;
    }

    /** @return \stdClass */
    public function getOrganizations()
    {
        return $this->stdOrganizations;
    }

    /** @return \stdClass */
    public function getTransformations()
    {
        return $this->stdTransformations;
    }
}
