<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Results;

interface IResultsFacade
{
    /**
     * @param  string $urlTeam
     * @return array  with keys lastMatch, nextMatch, currentTable
     */
    public function getTeamResults($urlTeam);

    /**
     * @param  string                               $urlTeam
     * @return \STM\Match\Preview\MatchPreview|null
     */
    public function getTeamMatchPreview($urlTeam);

    /**
     * @param  int      $idCompetition
     * @param  int|null $idTeam
     * @return array    with keys matches, tables
     */
    public function getCompetitionMatches($idCompetition, $idTeam = null);

    /**
     * @param  int                                                  $idCompetition
     * @return \STM\Competition\Season\Table\SeasonResultTable|null
     */
    public function getSeasonTable($idCompetition);

    /**
     * @param  int                                                     $idCompetition
     * @return \STM\Competition\Season\Table\SeasonTeamsPositions|null
     */
    public function getSeasonPositionInTable($idCompetition);

    /**
     *
     * @param  int                                           $idCompetition
     * @return \STM\Match\Stats\Action\MatchPlayerStats|null
     */
    public function getCompetitionPlayersStats($idCompetition);
}
