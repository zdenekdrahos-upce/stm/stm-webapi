<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Results;

use STM\Plugin\WebAPI\Competitions\ICompetitionsFacade;

//TODO: load cache without DB
class NullResults implements IResultsFacade
{
    /** @var \STM\Plugin\WebAPI\Competitions\ICompetitionsFacade */
    private $competitions;

    public function __construct(ICompetitionsFacade $competitions)
    {
        $this->competitions = $competitions;
    }

    public function competitions()
    {
        return $this->competitions;
    }

    public function getTeamResults($urlTeam)
    {
        return array(
            'currentCompetition' => null,
            'lastMatch' => null,
            'nextMatch' => null,
            'currentTable' => null
        );
    }

    public function getTeamMatchPreview($urlTeam)
    {
        return null;
    }

    public function getCompetitionMatches($idCompetition, $idTeam = null)
    {
        return array();
    }

    public function getSeasonTable($idCompetition)
    {
        return null;
    }

    public function getSeasonPositionInTable($idCompetition)
    {
        return array();
    }

    public function getCompetitionPlayersStats($idCompetition)
    {
        return null;
    }
}
