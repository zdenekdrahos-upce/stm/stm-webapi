<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Results;

use STM\Plugin\WebAPI\Competitions\ICompetitionsFacade;

class ResultsFacade implements IResultsFacade
{
    /** @var \STM\Plugin\WebAPI\Competitions\ICompetitionsFacade */
    private $competitions;
    /** @var \STM\Plugin\WebAPI\Results\TeamData */
    private $teamData;
    /** @var \STM\Plugin\WebAPI\Results\CompetitionData */
    private $competitionData;

    public function __construct(ICompetitionsFacade $competitions)
    {
        $this->competitions = $competitions;
        $this->teamData = new TeamData();
        $this->competitionData = new CompetitionData();
    }

    public function getTeamResults($urlTeam)
    {
        $this->setUrlTeam($urlTeam);
        $idTeam = $this->teamData->idTeam;;
        $currentCompetition = $this->competitions->getTeamCurrentCompetition($idTeam);
        $this->competitionData->idCompetition = $currentCompetition->id;

        return array(
            'currentCompetition' => $currentCompetition,
            'lastMatch' => $this->teamData->getLastMatch(),
            'nextMatch' => $this->teamData->getNextMatch(),
            'currentTable' => $this->competitionData->getSeasonTable()
        );
    }

    public function getTeamMatchPreview($urlTeam)
    {
        $this->setUrlTeam($urlTeam);

        return $this->teamData->getMatchPreview();
    }

    public function getCompetitionMatches($idCompetition, $idTeam = null)
    {
        $this->competitionData->idCompetition = $idCompetition;

        return $this->competitionData->getMatches($idTeam);
    }

    public function getSeasonTable($idCompetition)
    {
        $this->competitionData->idCompetition = $idCompetition;

        return $this->competitionData->getSeasonTable();
    }

    public function getSeasonPositionInTable($idCompetition)
    {
        $this->competitionData->idCompetition = $idCompetition;

        return $this->competitionData->getSeasonPositions();
    }

    public function getCompetitionPlayersStats($idCompetition)
    {
        $this->competitionData->idCompetition = $idCompetition;

        return $this->competitionData->getPlayersStats();
    }

    private function setUrlTeam($urlTeam)
    {
        $this->teamData->idTeam = $this->competitions->getIdTeam($urlTeam);
    }
}
