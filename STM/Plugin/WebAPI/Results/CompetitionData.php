<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Results;

use STM\StmCache;

class CompetitionData
{
    public $idCompetition;

    public function getMatches($idTeam)
    {
        $matches = StmCache::getAllCompetitionMatches($this->idCompetition);
        if (is_null($idTeam)) {
            return $matches;
        } else {
            return $this->filterTeamMatches($matches, $idTeam);
        }
    }

    private function filterTeamMatches($allMatches, $idTeam)
    {
        $matches = array();
        foreach ($allMatches as $match) {
            if ($match->getIdTeamHome() == $idTeam || $match->getIdTeamAway() == $idTeam) {
                $matches[] = $match;
            }
        }

        return $matches;
    }

    public function getPlayersStats()
    {
        return StmCache::getCompetitionPlayerStatistics($this->idCompetition);
    }

    public function getSeasonPositions()
    {
        return StmCache::getTeamsPositionsInTable($this->idCompetition);
    }

    public function getSeasonTable()
    {
        return StmCache::getSeasonTable($this->idCompetition);
    }
}
