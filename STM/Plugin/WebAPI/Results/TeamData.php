<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Results;

use STM\StmCache;
use STM\StmFactory;

class TeamData
{
    public $idTeam;

    public function getLastMatch()
    {
        return StmFactory::find()->Match->getLastPlayedMatch(null, $this->idTeam);
    }

    public function getNextMatch()
    {
        return StmFactory::find()->Match->getFirstUpcomingMatch(null, $this->idTeam);
    }

    public function getMatchPreview()
    {
        return StmCache::getUpcomingMatchPreview($this->idTeam);
    }

}
