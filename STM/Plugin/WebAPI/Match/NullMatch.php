<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Match;

class NullMatch implements IMatchFacade
{

    public function getMatchInfo($idMatch)
    {
        return null;
    }
}
