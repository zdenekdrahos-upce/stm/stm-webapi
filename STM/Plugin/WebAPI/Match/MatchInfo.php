<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Match;

class MatchInfo
{
    /** @var \STM\Match\Match */
    public $match;
    /** @var array */
    public $periods;
    /** @var \STM\Match\Lineup\Lineups */
    public $lineup;
    /** @var \STM\Match\Action\Player\MatchPlayerAction */
    public $playersActions;
    /** @var \STM\Prediction\Stats\PredictionStats */
    public $predictionsStats;
}
