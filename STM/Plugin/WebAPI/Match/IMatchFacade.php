<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Match;

interface IMatchFacade
{

    /**
     * @param  $idMatch
     * @return MatchInfo|null
     */
    public function getMatchInfo($idMatch);
}
