<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Match;

class MatchFacade implements IMatchFacade
{
    /** @var \STM\Plugin\WebAPI\Match\MatchData */
    private $matchData;

    public function __construct()
    {
        $this->matchData = new MatchData();
    }

    public function getMatchInfo($idMatch)
    {
        $this->matchData->setIdMatch($idMatch);

        return $this->matchData->getMatchInfo();
    }
}
