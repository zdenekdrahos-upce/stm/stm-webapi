<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Match;

use STM\Match\Lineup\Lineups;
use STM\StmFactory;

class MatchData
{
    /** @var boolean */
    private $isMatchLoaded;
    /** @var \STM\Match\Match */
    private $match;

    public function setIdMatch($idMatch)
    {
        $this->match = StmFactory::find()->Match->findById($idMatch);
        $this->isMatchLoaded = is_object($this->match);
    }

    public function getMatchInfo()
    {
        $matchInfo = new MatchInfo();
        if ($this->isMatchLoaded) {
            $matchInfo->match = $this->getMatch();
            $matchInfo->periods = $this->getPeriods();
            $matchInfo->lineup = $this->getLineups();
            $matchInfo->playersActions = $this->getPlayersActions();
            $matchInfo->predictionsStats = $this->getPredictionStats();
        }

        return $matchInfo;
    }

    public function getMatch()
    {
        if ($this->isMatchLoaded) {
            return $this->match;
        } else {
            return null;
        }
    }

    public function getPeriods()
    {
        if ($this->isMatchLoaded) {
            return $this->match->getPeriods();
        } else {
            return array();
        }
    }

    public function getLineups()
    {
        if ($this->isMatchLoaded) {
            return new Lineups($this->match);
        } else {
            return null;
        }
    }

    public function getPlayersActions()
    {
        if ($this->isMatchLoaded) {
            return StmFactory::find()->MatchPlayerAction->findByMatch($this->match);
        } else {
            return null;
        }
    }

    public function getPredictionStats()
    {
        if ($this->isMatchLoaded) {
            return StmFactory::find()->PredictionStats->findByMatch($this->match);
        } else {
            return null;
        }
    }
}
