<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

class CompetitionsList
{
    /** @var array */
    public $current = array();
    /** @var array */
    public $archive = array();

    public function __construct()
    {
        $this->current = array();
        $this->archive = array();
    }
}
