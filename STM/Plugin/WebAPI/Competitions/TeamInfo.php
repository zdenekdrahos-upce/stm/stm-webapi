<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

use \stdClass;

class TeamInfo
{
    /** @var int */
    public $id;
    /** @var string */
    public $name;
    /** @var string */
    public $url;

    public function __construct(stdClass $object)
    {
        $this->id = $object->idTeam;
        $this->name = $object->team;
        $this->url = $object->url;
    }
}
