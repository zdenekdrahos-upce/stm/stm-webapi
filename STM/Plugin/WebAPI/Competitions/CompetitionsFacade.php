<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

use \stdClass;

class CompetitionsFacade implements ICompetitionsFacade
{
    /** @var string */
    private $currentSeason;
    /** @var \STM\Plugin\WebAPI\Competitions\CompetitionsData */
    private $competitions;

    public function __construct(stdClass $object)
    {
        $this->currentSeason = $object->currentSeason;
        $this->competitions = new CompetitionsData($object->competitions);
        $this->competitions->setDefault($object->defaultIdTeam, $object->defaultIdCompetition);
    }

    public function getCurrentSeason()
    {
        return $this->currentSeason;
    }

    public function getIdDefaultCompetition()
    {
        return $this->competitions->defaultCompetition->id;
    }

    public function getTeams()
    {
        return $this->competitions->teams;
    }

    public function getIdTeam($urlTeam)
    {
        return $this->competitions->getTeam($urlTeam)->id;
    }

    public function getIdTeamFromCompetition($idCompetition)
    {
        return $this->competitions->getTeamFromCompetition($idCompetition)->id;
    }

    public function getCompetition($idCompetition)
    {
        return $this->competitions->getCompetition($idCompetition);
    }

    public function existsCompetition($idCompetition, $attribute = '')
    {
        return $this->competitions->existsCompetition($idCompetition, $attribute);
    }

    public function getTeamCurrentCompetition($idTeam)
    {
        return $this->competitions->getTeamCurrentCompetition($idTeam);
    }

    public function getTeamCurrentCompetitions($idTeam)
    {
        return $this->competitions->getTeamCurrentCompetitions($idTeam);
    }

    public function competitions($tag = '')
    {
        return $this->competitions->competitions($tag);
    }

    public function getAvailableTags()
    {
        return $this->competitions->getAvailableTags();
    }

    public function getTeamsWithCurrentCompetitions()
    {
        return $this->competitions->getTeamsWithCurrentCompetitions();
    }
}
