<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

interface ICompetitionsFacade
{

    public function getCurrentSeason();

    public function getIdDefaultCompetition();

    public function getTeams();

    public function getIdTeam($urlTeam);

    public function getIdTeamFromCompetition($idCompetition);

    public function getCompetition($idCompetition);

    public function existsCompetition($idCompetition, $attribute = '');

    /**
     * @param  int             $idTeam
     * @return CompetitionInfo
     */
    public function getTeamCurrentCompetition($idTeam);

    /**
     * @param  int             $idTeam
     * @return array
     */
    public function getTeamCurrentCompetitions($idTeam);

    public function getTeamsWithCurrentCompetitions();

    /**
     * @param  string           $tag
     * @return CompetitionsList
     */
    public function competitions($tag = '');

    /** @return array */
    public function getAvailableTags();
}
