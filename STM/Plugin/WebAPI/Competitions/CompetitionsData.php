<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

class CompetitionsData
{
    /** @var array */
    private $allCompetitions;
    /** @var array */
    public $teams;
    /** @var CompetitionInfo */
    public $defaultCompetition;
    /** @var TeamInfo */
    private $defaultTeam;
    /** @var CompetitionsList */
    private $all;
    /** @var array */
    private $tags;

    public function __construct(array $allCompetitions)
    {
        $this->teams = array();
        $this->tags = array();
        $this->allCompetitions = array();
        $this->all = new CompetitionsList();
        $this->loadCompetitions($allCompetitions);
    }

    public function setDefault($idTeam, $idCompetition)
    {
        $this->defaultTeam = $this->teams[$idTeam];
        $this->defaultCompetition = $this->allCompetitions[$idCompetition];
    }

    public function getTeams()
    {
        return $this->teams;
    }

    public function getTeam($urlTeam)
    {
        foreach ($this->allCompetitions as $competition) {
            if ($competition->team->url == $urlTeam) {
                return $competition->team;
            }
        }

        return $this->defaultTeam;
    }

    public function getTeamFromCompetition($idCompetition)
    {
        if ($this->existsCompetition($idCompetition)) {
            $competition = $this->allCompetitions[$idCompetition];

            return $competition->team;
        }

        return $this->defaultTeam;
    }

    public function getCompetition($idCompetition)
    {
        if ($this->existsCompetition($idCompetition)) {
            return $this->allCompetitions[$idCompetition];
        }

        return $this->defaultCompetition;
    }

    public function getTeamsWithCurrentCompetitions()
    {
        $teams = array();
        foreach ($this->all->current as $competition) {
            $teams[$competition->team->id] = $competition->team;
        }

        return $teams;
    }

    public function getTeamCurrentCompetition($idTeam)
    {
        $competitions = $this->getTeamCurrentCompetitions($idTeam);
        if ($competitions) {
            return array_shift($competitions);
        } else {
            return $this->defaultCompetition;
        }
    }

    public function getTeamCurrentCompetitions($idTeam)
    {
        $competitions = array();
        foreach ($this->all->current as $competition) {
            if ($competition->team->id == $idTeam) {
                $competitions[] = $competition;
            }
        }
        return $competitions;
    }

    public function getAvailableTags()
    {
        return array_keys($this->tags);
    }

    public function competitions($type = '')
    {
        if ($this->existsTag($type)) {
            return $this->tags[$type];
        }

        return $this->all;
    }

    public function existsCompetition($idCompetition, $tag = '')
    {
        if (array_key_exists($idCompetition, $this->allCompetitions)) {
            $competition = $this->allCompetitions[$idCompetition];
            if ($this->existsTag($tag)) {
                return $competition->hasAttribute($tag);
            } else {
                return true;
            }
        }
        return false;
    }

    private function existsTag($tag)
    {
        return array_key_exists($tag, $this->tags);
    }

    private function loadCompetitions($allCompetitions)
    {
        foreach ($allCompetitions as $team) {
            $teamInfo = new TeamInfo($team);
            $this->teams[$teamInfo->id] = $teamInfo;
            $comp = array(
                'current' => $team->current,
                'archive' => $team->archive
            );
            foreach ($comp as $key => $competitions) {
                foreach ($competitions as $competition) {
                    $isCurrent = $key == 'current';
                    $competitionInfo = new CompetitionInfo($competition, $teamInfo);
                    $this->allCompetitions[$competitionInfo->id] = $competitionInfo;
                    $this->addCompetition($this->all, $competitionInfo, $isCurrent);
                    foreach ($competition->tags as $tag) {
                        if (!array_key_exists($tag, $this->tags)) {
                            $this->tags[$tag] = new CompetitionsList();
                        }
                        $competitionInfo->tags[] = $tag;
                        $this->addCompetition($this->tags[$tag], $competitionInfo, $isCurrent);
                    }
                }
            }
        }
    }

    /**
     * @param CompetitionsList $list
     * @param CompetitionInfo  $competition
     * @param boolean          $isCurrent
     */
    private function addCompetition($list, $competition, $isCurrent)
    {
        if ($isCurrent) {
            $list->current[$competition->id] = $competition;
        } else {
            $teamName = $competition->team->name;
            $list->archive[$teamName][$competition->id] = $competition;
        }
    }
}
