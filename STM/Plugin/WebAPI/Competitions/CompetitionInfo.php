<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Competitions;

use \stdClass;

class CompetitionInfo
{

    /** @var int */
    public $id;
    /** @var string */
    public $name;
    /** @var \STM\Plugin\WebAPI\Competitions\TeamInfo */
    public $team;
    /** @var array */
    public $tags;

    public function __construct(stdClass $object, TeamInfo $team)
    {
        $this->id = $object->idCompetition;
        $this->name = $object->name;
        $this->team = $team;
        $this->tags = array();
    }

    public function hasAttribute($attribute)
    {
        return in_array($attribute, $this->tags, true);
    }
}
