<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Helpers;

/**
* Methods from https://bitbucket.org/treewec/treewec/src/tip/treewec/Treewec/Utils/Comparators.php
*/
class Comparers
{

    public function compareUTF8Strings($a, $b)
    {
        $result = self::compareStringsAndNumbers($a, $b);

        return is_null($result) ? strcoll($a, $b) : $result;
    }

    private function compareStringsAndNumbers($a, $b)
    {
        $isNumericA = is_numeric($a);
        $isNumericB = is_numeric($b);
        $isStringA = is_string($a);
        $isStringB = is_string($b);
        if ($isNumericA && $isNumericB) {
            return self::compareNumbers($a, $b);
        } elseif ($isNumericA && $isStringB) {
            return 1;
        } elseif ($isNumericB && $isStringA) {
            return -1;
        } elseif ($isStringA && $isStringB) {
            return null;
        } else {
            return 1; // invalid arguments
        }
    }
}
