<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Helpers;

class StmEntities
{

    public static function getObjectProperty($object, $key)
    {
        $array = $object->toArray();

        return $array[$key];
    }
}
