<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations\Members;

use STM\Plugin\WebAPI\Helpers\Comparers;
use STM\Plugin\WebAPI\Helpers\StmEntities;

class PersonComparer
{
    /** @var STM\Plugin\WebAPI\Helpers\Comparers */
    private $comparers;
    /** @var array */
    private $sortedPositions;
    /** @var string */
    private $keyPosition;
    /** @var string */
    private $keyPerson;

    public function __construct()
    {
        $this->comparers = new Comparers();
        $this->keyPosition = 'position';
        $this->keyPerson = 'keyPerson';
    }

    public function setSortedPositions(array $sortedPositions)
    {
        $this->sortedPositions = $sortedPositions;
    }

    public function setKeyPerson($keyPerson)
    {
        $this->keyPerson = $keyPerson;
    }

    public function compareMembers($a, $b)
    {
        $positionA = StmEntities::getObjectProperty($a, $this->keyPosition);
        $positionB = StmEntities::getObjectProperty($b, $this->keyPosition);
        if ($positionA == $positionB) {
            return $this->comparePersonsByName($a, $b);
        } else {
            $indexA = array_search($positionA, $this->sortedPositions);
            $indexB = array_search($positionB, $this->sortedPositions);

            return $indexA > $indexB ? 1 : -1;
        }
    }

    /**
     * @param  object $a TeamMember or ClubMember
     * @param  object $b
     * @return int
     */
    private function comparePersonsByName($a, $b)
    {
        $personA = StmEntities::getObjectProperty($a, $this->keyPerson);
        $personB = StmEntities::getObjectProperty($b, $this->keyPerson);
        $namesA = explode(' ', $personA);
        $namesB = explode(' ', $personB);
        if ($namesA[1] != $namesB[1]) {
            return $this->comparers->compareUTF8Strings($namesA[1], $namesB[1]);
        } else {
            return $this->comparers->compareUTF8Strings($namesA[0], $namesB[0]);
        }
    }
}
