<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations\Members;

use \stdClass;

class MembersOrder
{
    /** @var \STM\Plugin\WebAPI\Transformations\Members\PersonComparer */
    private $comparer;
    /** @var array */
    private $teamOrder;
    /** @var array */
    private $clubOrder;

    public function __construct(stdClass $stdOrder)
    {
        $this->teamOrder = $stdOrder->teams;
        $this->clubOrder = $stdOrder->clubs;
        $this->comparer = new PersonComparer();
    }

    public function sortTeamMembers(array $teamMembers)
    {
        $this->comparer->setKeyPerson('person');
        $this->comparer->setSortedPositions($this->teamOrder);

        return $this->sortMembers($teamMembers);
    }

    public function sortClubMembers(array $clubMembers)
    {
        $this->comparer->setKeyPerson('person');
        $this->comparer->setSortedPositions($this->clubOrder);

        return $this->sortMembers($clubMembers);
    }

    public function sortMatchPlayers(array $matchPlayers)
    {
        $this->comparer->setKeyPerson('player');
        $this->comparer->setSortedPositions($this->teamOrder);

        return $this->sortMembers($matchPlayers);
    }

    private function sortMembers(array $members)
    {
        if ($members) {
            usort($members, array($this->comparer, 'compareMembers'));
        }

        return $members;
    }
}
