<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations;

use \stdClass;
use STM\Plugin\WebAPI\Transformations\Members\MembersOrder;
use STM\Plugin\WebAPI\Transformations\PlayersStats\PlayerStatsTransformation;
use STM\Plugin\WebAPI\Transformations\Match\TeamInMatch;

class TransformationsFacade implements ITransformationsFacade
{
    /** @var stdClass */
    private $object;
    /** @var \STM\Plugin\WebAPI\Transformations\PlayersStats\PlayerStatsTransformation */
    private $playerStats;
    /** @var \STM\Plugin\WebAPI\Transformations\Members\MembersOrder */
    private $membersOrder;
    /** @var \STM\Plugin\WebAPI\Transformations\Match\TeamInMatch */
    private $teamInMatch;

    public function __construct(stdClass $object)
    {
        $this->object = $object;
    }

    public function transformPlayersStats($stats)
    {
        if (is_null($this->playerStats)) {
            $this->playerStats = new PlayerStatsTransformation($this->object->stats);
        }

        return $this->playerStats->transform($stats);
    }

    public function sortTeamMembers(array $teamMembers)
    {
        $this->checkMembersOrder();

        return $this->membersOrder->sortTeamMembers($teamMembers);
    }

    public function sortClubMembers(array $clubMembers)
    {
        $this->checkMembersOrder();

        return $this->membersOrder->sortClubMembers($clubMembers);
    }

    private function checkMembersOrder()
    {
        if (is_null($this->membersOrder)) {
            $this->membersOrder = new MembersOrder($this->object->membersOrder);
        }
    }

    public function transformMatchInfo($matchInfo, $idTeam)
    {
        if (is_null($this->teamInMatch)) {
            $this->checkMembersOrder();
            $this->teamInMatch = new TeamInMatch($this->membersOrder, $this->object->matchActions);
        }

        return $this->teamInMatch->transform($matchInfo, $idTeam);
    }
}
