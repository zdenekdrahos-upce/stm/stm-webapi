<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations\Match;

use STM\Match\Match;
use STM\Match\ExtendedMatch;
use STM\Match\Lineup\Lineups;
use STM\Plugin\WebAPI\Match\MatchInfo;
use STM\Plugin\WebAPI\Transformations\Members\MembersOrder;

class TeamInMatch
{

    /** @var \STM\Match\Match */
    private $match;
    /** @var int */
    private $idTeam;
    /** @var array */
    private $result;
    /** @var \STM\Plugin\WebAPI\Transformations\Members\MembersOrder */
    private $playersOrder;
    /** @var array */
    private $matchActions;

    public function __construct(MembersOrder $order, array $matchActions)
    {
        $this->playersOrder = $order;
        $this->matchActions = $matchActions;
    }

    public function transform($matchInfo, $idTeam)
    {
        $this->initResult();
        if ($matchInfo instanceof MatchInfo && $matchInfo->match instanceof Match) {
            $this->idTeam = $idTeam;
            $this->match = $matchInfo->match;
            $this->loadScores();
            $this->loadLineup($matchInfo->lineup);
            $this->loadPlayersActions($matchInfo->playersActions);
        }

        return $this->result;
    }

    private function loadScores()
    {
        $extendedMatch = new ExtendedMatch($this->match, $this->idTeam);
        $this->result['score'] = $extendedMatch->score;
        $this->result['result'] = $extendedMatch->result;
    }

    private function loadLineup(Lineups $lineups)
    {
        $teamLineupInMatch = $this->getTeamLineup($lineups);
        $lineupPlayers = array();
        $substitutionPlayers = array();
        foreach ($teamLineupInMatch as $player) {
            $playerArray = $player->toArray();
            if ($playerArray['minute_in'] != '0') {
                $substitutionPlayers[] = $playerArray['player'];
                continue;
            }
            $name = $playerArray['player'];
            $idSubst = $player->getIdSubstitutionPlayer();
            if ($idSubst) {
                $subst = $lineups->getSubstitutionPlayer($idSubst);
                $substArray = $subst->toArray();
                $name .= " ({$substArray ['player']})";
            }
            $lineupPlayers[$playerArray['position']][] = $name;
        }
        foreach ($lineupPlayers as $position => $players) {
            $lineupPlayers[$position] = implode(', ', $players);
        }
        $this->result['teamLineup'] = array(
            'lineup' => implode(' - ', $lineupPlayers),
            'substitution' => implode(', ', $substitutionPlayers),
        );
    }

    private function getTeamLineup($lineup)
    {
        $teamLineup = array();
        if ($this->match->getIdTeamHome() == $this->idTeam) {
            $teamLineup = $lineup->getLineupHome();
        } elseif ($this->match->getIdTeamAway() == $this->idTeam) {
            $teamLineup = $lineup->getLineupAway();
        }

        return $this->playersOrder->sortMatchPlayers($teamLineup);
    }

    private function loadPlayersActions(array $playersActions)
    {
        foreach ($playersActions as $action) {
            $arr = $action->toArray();
            $actionName = $arr['name_action'];
            $playerName = $arr['name_player'];
            $key = $this->getMatchActionKey($actionName);
            $this->result['playersActions'][$key][] = $playerName;
        }
        $this->deleteEmptyActions();
    }

    private function getMatchActionKey($playerAction)
    {
        foreach ($this->matchActions as $action) {
            if ($action->matchAction == $playerAction) {
                return $action->heading;
            }
        }

        return $playerAction;
    }

    private function deleteEmptyActions()
    {
        $keys = array_keys($this->result['playersActions']);
        foreach ($keys as $action) {
            if (empty($this->result['playersActions'][$action])) {
                unset($this->result['playersActions'][$action]);
            }
        }
    }

    private function initResult()
    {
        $this->result = array(
            'score' => '-:-',
            'result' => '?',
            'teamLineup' => array(
                'lineup' => array(),
                'substitution' => array(),
            ),
            'playersActions' => array()
        );
        foreach ($this->matchActions as $action) {
            $this->result['playersActions'][$action->heading] = array();
        }
    }

}
