<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations\PlayersStats;

use STM\Match\Stats\Action\MatchPlayerStats;

class PlayerStatsTransformation
{
    /** @var array */
    private $statisticsInfo;
    /** @var \STM\Match\Stats\Action\MatchPlayerStats */
    private $stats;
    /** @var array */
    private $players;
    /** @var array */
    private $actionsWithPlayers;
    /** @var array */
    private $result;

    public function __construct(array $statisticsNames)
    {
        $this->statisticsInfo = array();
        foreach ($statisticsNames as $stat) {
            $this->statisticsInfo[$stat->matchAction] = $stat;
        }
    }

    public function transform($stats)
    {
        $this->initResult();
        if ($stats instanceof MatchPlayerStats) {
            $this->stats = $stats;
            $this->loadActionsAndPlayers();
            $this->sortAttributes();
            $this->convertToOutputFormat();
        }

        return $this->result;
    }

    private function initResult()
    {
        $this->result = array();
        foreach ($this->statisticsInfo as $stat) {
            $this->result[$stat->heading] = $stat->onEmpty;
        }
    }

    private function loadActionsAndPlayers()
    {
        $this->players = array();
        $this->actionsWithPlayers = array();
        foreach ($this->stats->getStatistics() as $stat) {
            $idMember = $stat['member']->getId();
            $memberArray = $stat['member']->toArray();
            $this->players[$idMember] = $memberArray['person'];
            $this->actionsWithPlayers['playedMatches'][$idMember] = $stat['general']->playedMatches;
            if (isset($stat['actions'])) {
                foreach ($stat['actions'] as $action) {
                    if (array_key_exists($action->matchAction, $this->statisticsInfo)) {
                        $this->actionsWithPlayers[$action->matchAction][$idMember] = $action->count;
                    }
                }
            }
        }
    }

    private function sortAttributes()
    {
        foreach ($this->actionsWithPlayers as &$playerActions) {
            arsort($playerActions);
        }
    }

    private function convertToOutputFormat()
    {
        foreach ($this->actionsWithPlayers as $idAction => $values) {
            $players = array();
            foreach ($values as $idPlayer => $count) {
                $players[$count][] = $this->players[$idPlayer];
            }
            $key = $this->statisticsInfo[$idAction]->heading;
            $this->result[$key] = $players;
        }
    }
}
