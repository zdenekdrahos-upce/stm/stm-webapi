<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI\Transformations;

interface ITransformationsFacade
{
    /**
     *
     * @param  \STM\Match\Stats\Action\MatchPlayerStats $stats
     * @return array
     */
    public function transformPlayersStats($stats);

    /**
     * @param  array $teamMembers
     * @return array
     */
    public function sortTeamMembers(array $teamMembers);

    /**
     * @param  array $teamMembers
     * @return array
     */
    public function sortClubMembers(array $clubMembers);

    /**
     *
     * @param  \STM\Plugin\WebAPI\Match\MatchInfo $matchInfo
     * @param  type                               $idTeam
     * @return array                              with keys score, result (W,L,D), teamLineup (array with keys lineup, substitution),
     *   playersActions (array where key = actionName and value = playerName)
     */
    public function transformMatchInfo($matchInfo, $idTeam);
}
