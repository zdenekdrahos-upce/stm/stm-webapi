<?php
/*
 * Sports Table Manager (https://bitbucket.org/stm-sport)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace STM\Plugin\WebAPI;

interface IStmAPI
{

    /**
     * @param  int     $idUser
     * @return boolean
     */
    public function isUserWebAdministrator($idUser);

    /** @return \STM\Plugin\WebAPI\Competitions\ICompetitionsFacade */
    public function competitions();

    /** @return \STM\Plugin\WebAPI\Results\IResultsFacade */
    public function results();

    /** @return \STM\Plugin\WebAPI\Match\IMatchFacade */
    public function match();

    /** @return \STM\Plugin\WebAPI\Members\IMembersFacade */
    public function members();

    /** @return \STM\Plugin\WebAPI\Transformations\ITransformationsFacade */
    public function transformations();
}
