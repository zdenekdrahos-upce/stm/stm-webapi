<?php
require_once(__DIR__ . '/autoload.php');

define('STM_WEBAPI_CONFIG', __DIR__ . '/stm.json');
define('STM_WEBAPI_BOOTSTRAP_DIR', __DIR__ . '/');

$stmApi = \STM\Plugin\WebAPI\StmAPIProxy::getInstance();
$teamMembers = $stmApi->members()->getTeamMembers();
echo var_dump($stmApi->transformations()->sortTeamMembers($teamMembers));
$clubMembers = $stmApi->members()->getClubMembers();
echo var_dump($stmApi->transformations()->sortClubMembers($clubMembers));
echo '<hr />';

$competitions = $stmApi->competitions();
echo var_dump($competitions->getCurrentSeason());
echo var_dump($competitions->getTeams());
echo '<hr />';

$attributes = $competitions->getAvailableTags();
echo var_dump($attributes);
echo var_dump($competitions->competitions());
echo var_dump($competitions->competitions('playersStats'));
echo var_dump($competitions->competitions('tablePositions'));
$idCompetition = 2;
$competition = $competitions->getCompetition($idCompetition);
echo var_dump($competition);
echo var_dump($competition->hasAttribute('playersStats'));
echo var_dump($competition->hasAttribute('tablePositions'));
echo '<hr />';
echo var_dump($competitions->getTeamsWithCurrentCompetitions());
echo '<hr />';

echo var_dump($competitions->getIdTeam('muzi'));
echo var_dump($competitions->getIdTeam('dorost'));
echo var_dump($competitions->getIdTeam('zaci'));
echo var_dump($competitions->getIdTeam('becko'));
echo var_dump($competitions->getIdTeam('die'));
echo '<hr />';
echo var_dump($competitions->getTeamCurrentCompetition(1));
echo var_dump($competitions->getTeamCurrentCompetition(2));
echo var_dump($competitions->getTeamCurrentCompetition(91));
echo var_dump($competitions->getTeamCurrentCompetition(3));
echo '<br />';
echo var_dump($competitions->getTeamCurrentCompetitions(1));
echo '<hr />';
echo var_dump($competitions->getIdTeamFromCompetition(1));
echo var_dump($competitions->getIdTeamFromCompetition(2));
echo var_dump($competitions->getIdTeamFromCompetition(14));
echo var_dump($competitions->getIdTeamFromCompetition(10));
echo '<hr />';

$data = $stmApi->results();
echo var_dump($data->getTeamResults('muzi'));
echo '<hr />';
echo var_dump($data->getTeamMatchPreview('muzi'));
echo '<hr />';
echo var_dump($data->getSeasonTable(8));
echo '<hr />';
echo var_dump($data->getCompetitionMatches(8));
echo '<hr />';

echo 'ONLY TEAM MATCHES ID = 1<br >';
echo var_dump($data->getCompetitionMatches(8, 1));
echo '<hr />';
echo var_dump($data->getSeasonPositionInTable(8, 1));
echo '<hr />';

$match = $stmApi->match();
$matchInfo = $match->getMatchInfo(6);
echo var_dump($matchInfo);
echo var_dump($stmApi->transformations()->transformMatchInfo($matchInfo, 1));
echo '<hr />';

$data = $stmApi->results();
$stats = $data->getCompetitionPlayersStats(8);
echo var_dump($stats);
echo '<hr />';

$transform = $stmApi->transformations();
echo var_dump($transform->transformPlayersStats($stats));
echo '<hr />';
?>